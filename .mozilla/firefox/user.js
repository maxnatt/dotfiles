// >>> Main source: https://github.com/pyllyukko/user.js <<< \\

// >>> General browser settings <<< \\

user_pref("keyword.enabled", true);
// Do not hide protocols
user_pref("browser.urlbar.trimURLs", false);
// Do not automatically send selection to clipboard on some Linux platforms
user_pref("clipboard.autocopy",	false);
// Make typos more visible
user_pref("ui.SpellCheckerUnderlineStyle", 4);
// Load pinned tabs only when clicking on them
user_pref("browser.sessionstore.restore_pinned_tabs_on_demand", true);
// Do not paste on middle click
user_pref("middlemouse.paste", false);
// Don't hide tabs when in fullscreen
user_pref("browser.fullscreen.autohide", false);
// Do not check if Firefox is the default browser
user_pref("browser.shell.checkDefaultBrowser", false);
// Make URL bar behave like Firefox on Windows regarding mouse clicks
user_pref("browser.urlbar.clickSelectsAll", true);
user_pref("browser.urlbar.doubleClickSelectsAll", false);
user_pref("layout.word_select.stop_at_punctuation", true);
// Disable notifications feature
// user_pref("dom.webnotifications.enabled", false);
// Make backspace work again
user_pref("browser.backspace_action", 0);
// Don't sync the state of addons (enabled/disabled)
user_pref("services.sync.addons.ignoreUserEnabledChanges", true);
// Stop Firefox moving itself to a different workspace
user_pref("widget.disable-workspace-management", true);
user_pref("widget.workspace-management", false);
// Use KDE Plasma file dialog
user_pref("widget.use-xdg-desktop-portal", true);

// >>> Network <<< \\

// Disable DNS prefetching
user_pref("network.dns.disablePrefetch", true);
user_pref("network.dns.disablePrefetchFromHTTPS", true);
// Enforce Trusted Recursive Resolver (Cloudflare DNS)
user_pref("network.trr.mode", 2);
user_pref("network.trr.bootstrapAddress", "1.1.1.1");
// Enable ESNI
user_pref("network.security.esni.enabled", true);
// Reject .onion hostnames before passing them to DNS
user_pref("network.dns.blockDotOnion", true);
// Don't try to guess domain names when entering an invalid domain name in URL bar
user_pref("browser.fixup.alternate.enabled", false);
// Disable leaking network/browser connection information via Javascript
user_pref("dom.netinfo.enabled", false);
// Disable "beacon" asynchronous HTTP transfers (used for analytics)
user_pref("beacon.enabled", false);
// Disable video stats to reduce fingerprinting threat
user_pref("media.video_stats.enabled", false);
// Prevent the browser from making requests to sites outside of the primary domain from the site
user_pref("privacy.firstparty.isolate", true);
// Don't reveal internal IP when using WebRTC
user_pref("media.peerconnection.ice.no_host", true);
// Disable prefetching of <link rel="next"> links
user_pref("network.prefetch-next", false);
// Disable pinging URIs specified in HTML <a> ping= attributes
user_pref("browser.send_pings", false);
// When browser pings are enabled, only allow pinging the same host as the origin page
user_pref("browser.send_pings.require_same_host", true);
// Disable SSDP
user_pref("browser.casting.enabled", false);
// Disable the predictive service (Necko)
user_pref("network.predictor.enabled", false);

// >>> Location/locales <<< \\

// Disable GeoIP lookup on your address to set default search engine region
user_pref("browser.search.countryCode", "US");
user_pref("browser.search.region", "US");
user_pref("browser.search.geoip.url", "");
// Prevent leaking application locale/date format using JavaScript
user_pref("javascript.use_us_english_locale", true);
// Set Accept-Language HTTP header to en-US regardless of Firefox localization
user_pref("intl.accept_languages", "en-us, en");
// Don't use OS values to determine locale, force using Firefox locale setting
user_pref("intl.locale.matchOS", false);
// Don't use Mozilla-provided location-specific search engines
user_pref("browser.search.geoSpecificDefaults", false);
// When geolocation is enabled, don't log geolocation requests to the console
user_pref("geo.wifi.logging.enabled", false);

// >>> WebGL <<< \\

// When webGL is enabled, use the minimum capability mode
// user_pref("webgl.min_capability_mode", true);
// When webGL is enabled, disable webGL extensions
// user_pref("webgl.disable-extensions", true);
// When webGL is enabled, force enabling it even when layer acceleration is not supported
// user_pref("webgl.disable-fail-if-major-performance-caveat", true);
// When webGL is enabled, do not expose information about the graphics driver
// user_pref("webgl.enable-debug-renderer-info", false);

// >>> Cookies <<< \\

user_pref("services.sync.prefs.sync.network.cookie.cookieBehavior", false);
// Make sure that third-party cookies (if enabled) never persist beyond the session.
user_pref("network.cookie.thirdparty.sessionOnly", true);

// >>> Security <<< \\

// Disable TLS Session Tickets
user_pref("security.ssl.disable_session_identifiers", true);
// Enforce Public Key Pinning
user_pref("security.cert_pinning.enforcement_level", 2);
// Disallow SHA-1
user_pref("security.pki.sha1_enforcement_level", 1);
// Warn the user when server doesn't support RFC 5746 ("safe" renegotiation)
user_pref("security.ssl.treat_unsafe_negotiation_as_broken", true);
// Disable TLS 0-RTT
// https://www.privateinternetaccess.com/blog/2018/11/supercookey-a-supercookie-built-into-tls-1-2-and-1-3/
// user_pref("security.tls.enable_0rtt_data", false);
// Disable TLS session identifiers
user_pref("security.ssl.disable_session_identifiers", true);
// Ensure you have a security delay when installing add-ons (milliseconds)
user_pref("security.dialog_enable_delay", 1000);
// Enforce Mixed Active Content Blocking
user_pref("security.mixed_content.block_active_content", true);
// Disable remote debugging
user_pref("devtools.debugger.remote-enabled", false);
user_pref("devtools.debugger.force-local", true);
// Disallow NTLMv1
user_pref("network.negotiate-auth.allow-insecure-ntlm-v1",	false);
// Show in-content login form warning UI for insecure login fields
user_pref("security.insecure_field_warning.contextual.enabled", true);
// Enable insecure password warnings (login forms in non-HTTPS pages)
user_pref("security.insecure_password.ui.enabled", true);
// Enable HSTS preload list (pre-set HSTS sites list provided by Mozilla)
user_pref("network.stricttransportsecurity.preloadlist", true);

// >>> Other <<< \\

// Disable formless login capture
user_pref("signon.formlessCapture.enabled", false);
// Headers tweaking
user_pref("network.http.sendRefererHeader", 2);
// Prevent font fingerprinting
// user_pref("browser.display.use_document_fonts", 0);
// Enable hardening against various fingerprinting vectors (Tor Uplift project)
user_pref("privacy.resistFingerprinting", true);
// Enable Firefox Tracking Protection
user_pref("privacy.trackingprotection.enabled", true);
user_pref("privacy.trackingprotection.pbmode.enabled", true);
// Enable contextual identity Containers feature (Firefox >= 52)
user_pref("privacy.userContext.enabled", true);
// Disable querying Google Application Reputation database for downloaded binary files
user_pref("browser.safebrowsing.downloads.remote.enabled", false);

// Disable DOM timing API
user_pref("dom.enable_performance", false);
// Make sure the User Timing API does not provide a new high resolution timestamp
user_pref("dom.enable_user_timing", false);
// Disable resource timing API
user_pref("dom.enable_resource_timing", false);
// Disable Web Audio API
// user_pref("dom.webaudio.enabled", false);  
// Disable access to some devices
user_pref("dom.gamepad.enabled", false);  
user_pref("dom.vr.enabled", false);
user_pref("dom.vibrator.enabled", false);
// Disable telephony API
user_pref("dom.telephony.enabled", false);
// Disable speech recognition
user_pref("media.webspeech.recognition.enable", false);
// Disable speech synthesis
// user_pref("media.webspeech.synth.enabled", false);
// Disable sensor API
user_pref("device.sensors.enabled", false);
// Disable face detection
user_pref("camera.control.face_detection.enabled", false);

// >>> Hardware Video Acceleration <<< \\
user_pref("media.ffmpeg.vaapi.enabled", true);
user_pref("media.ffvpx.enabled", false);
user_pref("media.navigator.mediadatadecoder_vpx_enabled", true);
user_pref("media.rdd-vpx.enabled", false);

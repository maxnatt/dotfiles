set background=dark
let g:airline_theme='ayu_mirage'
let g:ayucolor='mirage'
colorscheme ayu
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=#343c48 ctermbg=3
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=#343c48 ctermbg=4

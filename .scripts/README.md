# Scripts

### wallpaper_changer_normal.sh

This is Bash script to change the desktop wallpaper using **feh** and automatically blur it using **ImageMagick** to use, for instance, on your lockscreen. Just set your lockscreen to use `~/.bg.jpg` file which will be updated every time after running this script. The `wallpaper_changer.sh` uses lock mechanism, so if you run it before your previous instance has closed (blurring may take some time, although current settings are quite fast), the script will close that instance first not to waste resources and overwrite images by accident.

### wallpaper_changer_dynamic.sh

Bash script to set wallpapers using **feh** based on time. Wallpapers are available in the [maxnatt/dynamic_wallpapers](https://gitlab.com/maxnatt/dynamic_wallpapers) repository.

### rmshit.py

Very useful Python script to clean cache in a home directory. Taken from [this](https://github.com/lahwaacz/Scripts) repository.
